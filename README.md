# Telefoni Sicuri

Una guida in puro testo con formattazione markdown, convertibile in PDF
via pandoc e stampabile in formato pieghevole. 


[criptolibretto PDF A4](book-a4.pdf)
[criptolibretto PDF A5 impaginato](book-a5.pdf)

Per ottenere un PDF da stampare, è anche possibile compilare il libro
seguendo le istruzioni.

## Compilare il Pdf

### Requisiti

Assicurati di avere i seguenti programmi:

* make
* pandoc
* texlive-latex-base
* pdflatex (texlive-latex-recommended)
* psutils
* texlive-fonts-recommended

### Generazione

Principali comandi:

* `make book-a4.pdf` -- Genera un PDF in formato A4
* `make book-a5.pdf` -- Genera un PDF in formato A5 adatto alla stampa
* `make clean` -- Pulisce la directory rimuovendo tutti i file generati

Per ottenere una lista completa dei comandi disponibili esegui `make help`.

## Stampa

Stampa il file: `book-a5.pdf`

Selfprint: stampa normale in portait mode, ma imbocca le pagine a mano
(lato da stampare UP) e quando le ricevi (le ricevi lato bianco in alto)
non è necessario girarle ma basta abbassare e imboccare di nuovo.

### Markdown

* Guida di riferimento: [Pandoc's Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown)

